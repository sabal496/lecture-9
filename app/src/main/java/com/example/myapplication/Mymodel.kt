package com.example.myapplication

import android.os.Parcel
import android.os.Parcelable

class Mymodel(val username:String?,val firstName:String?,val LastName:String?,val Age:Int?,val Adress:String?=""):Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readValue(Int::class.java.classLoader) as? Int,
        parcel.readString()
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(username)
        parcel.writeString(firstName)
        parcel.writeString(LastName)
        parcel.writeValue(Age)
        parcel.writeString(Adress)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Mymodel> {
        override fun createFromParcel(parcel: Parcel): Mymodel {
            return Mymodel(parcel)
        }

        override fun newArray(size: Int): Array<Mymodel?> {
            return arrayOfNulls(size)
        }
    }

}