package com.example.myapplication

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_choose_address.*

class ChooseAddressActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_choose_address)
        init()
    }

    private  fun init(){
        sendadress.setOnClickListener {

            sendAdress(adresstext.text.toString())
        }
    }
    private  fun sendAdress(adress:String){
        val intent=Intent(this,RegisterActiviry::class.java)
        intent.putExtra("Adress",adress)
        startActivity(intent)

    }

}
